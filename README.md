
# Caretaker Robot for the Elderly - Web Backend
Developed by [Rajeev KARUVATH][df2]

A brief overview of the project [here][df1]

# Main Dashboard

[![N|Solid](https://blog.rajeevkr.me/wp-content/uploads/2018/11/django-1-1024x576.jpg)](https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly/)

# Patient Profile

[![N|Solid](https://blog.rajeevkr.me/wp-content/uploads/2018/11/django-2-1024x576.jpg)](https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly/)

# Medicine Compartment Edit

[![N|Solid](https://blog.rajeevkr.me/wp-content/uploads/2018/11/django-3-1024x576.jpg)](https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly/)

# Medicine Compartment Inventory

[![N|Solid](https://blog.rajeevkr.me/wp-content/uploads/2018/11/django-4-1024x576.jpg)](https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly/)

# Livestream

[![N|Solid](https://blog.rajeevkr.me/wp-content/uploads/2018/11/django-5-1024x576.jpg)](https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly/)

[df1]: <https://blog.rajeevkr.me/2018/11/27/caretaker-robot-for-the-elderly>
[df2]: <https://rajeevkr.me>
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class VitalSings(models.Model):

    bpm = models.IntegerField(default=0)
    temp = models.IntegerField(default=0)
    gx = models.IntegerField(default=0)
    gy = models.IntegerField(default=0)
    gz = models.IntegerField(default=0)
    timeStamp = models.DateTimeField('VS Timestamp')
    def __str__(self):


    	return str(self.timeStamp)

class MedicineCompartment(models.Model):
	compartmentNo = models.IntegerField(default=0,primary_key=True)
	medicineName = models.CharField(max_length=200)
	medicineColor = models.CharField(max_length=200)
	timeArray = models.CharField(max_length=200)
	def __str__(self):
		return "Compartment No " + str(self.compartmentNo)
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),	
    url(r'^update/$', views.update, name='update'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^stream/$', views.stream, name='stream'),
    url(r'^streamRaw/$', views.getRawStream, name='getRawStream'),
    url(r'^medicine/$', views.medicine, name='medicine'),
    url(r'^(?P<cpt_number>[0-9]+)/prescriptions/$', views.prescriptions, name='prescriptions'),
    url(r'^(?P<cpt_number>[0-9]+)/getprescription/$', views.getPrescription, name='getPrescription'),
    url(r'^(?P<cpt_number>[0-9]+)/getlatesttime/$', views.getUpcomingMedicineForCompartment, name='getUpcomingMedicineForCompartment'),
    url(r'^updateprescription/$', views.updatePrescription, name='updatePrescription')
]
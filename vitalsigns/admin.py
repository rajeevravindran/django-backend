from django.contrib import admin

from .models import VitalSings
from .models import MedicineCompartment

admin.site.register(VitalSings)
admin.site.register(MedicineCompartment)
# Register your models here.

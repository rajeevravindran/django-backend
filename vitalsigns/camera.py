import cv2
import numpy as np
class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
        self.video.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,320)
        self.video.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,240)
    
    def __del__(self):
        self.video.release()
    
    def get_frame(self):
        success, image = self.video.read()
        cannyT= cv2.Canny(image,50,500)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #Concatenation requires same dimensions mats. Hence to concatenate with Canny output (single dim)
        vis = np.concatenate((gray, cannyT), axis=1)
        ret, jpeg = cv2.imencode('.jpg', vis)
        return jpeg.tobytes()

def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


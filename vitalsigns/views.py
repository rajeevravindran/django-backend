from django.shortcuts import render
from django.http import HttpResponse,StreamingHttpResponse,JsonResponse,HttpResponseBadRequest
from serialReader import randomVitalSignsGenerator
from serialReader import readSerially
from django.shortcuts import render
import json
from vitalsigns.models import VitalSings,MedicineCompartment
from django.utils import timezone
from camera import VideoCamera, gen
import datetime,operator

def index(request):
    return render(request,'vitalsigns/index.html')

def profile(request):
    return render(request,'vitalsigns/profile.html')

def medicine(request):
    allTimes = []
    for i in range(1,7):
        prescriptionDetails = MedicineCompartment.objects.filter(compartmentNo = i)[0]
        upcomingTime = getUpcomingMedicine(formatTimeIntoArray(prescriptionDetails.timeArray))
        print upcomingTime
        timeResponse = {
            "hour" : upcomingTime[0],
            "minute" : upcomingTime[1]
        }
        allTimes.append(timeResponse)
    return render(request,'vitalsigns/medicine.html',{"upcomingTime" : allTimes})

def formatTimeIntoArray(timeArray):
    #01:30 AM,01:30 AM,
    raw = timeArray.split(",")
    FinalArray = []
    for item in raw:
        try:
            time,meridien = item.split(" ")
        except:
            continue
        hours = int(time.split(":")[0])
        minutes = int(time.split(":")[1])
        if meridien == "PM":
            hours = hours + 12
        if meridien == "AM" and hours == 12:
            hours = hours - 12
        FinalArray.append([hours,minutes])
    return FinalArray

def formatTimeForPicker(timeArray):
    return timeArray[:-1].split(",")

def getUpcomingMedicine(timeArray):
    minutesArray = []
    for eachTime in timeArray:
        minutesArray.append(eachTime[0]*60+eachTime[1])
    #print "Recieved Time :",timeArray,"in minutes",minutesArray
    now = datetime.datetime.now()
    currentMinutes = now.hour*60+now.minute
    minutesArray[:] = [x - currentMinutes for x in minutesArray]
    for i in range(0,len(minutesArray)):
        if minutesArray[i] < 0:
            minutesArray[i] = 1440 - minutesArray[i]*-1
    #print "Current Time :",[now.hour,now.minute],"in minutes",currentMinutes
    #print "After deducting :",minutesArray
    #print timeArray[minutesArray.index(min(minutesArray))]
    return timeArray[minutesArray.index(min(minutesArray))]

def getUpcomingMedicineForCompartment(request,cpt_number):
    try:
        prescriptionDetails = MedicineCompartment.objects.filter(compartmentNo = cpt_number)[0]
        upcomingTime = getUpcomingMedicine(formatTimeIntoArray(prescriptionDetails.timeArray))
        print upcomingTime
        timeResponse = {
            "hour" : upcomingTime[0],
            "minute" : upcomingTime[1]
        }
        print timeResponse
        return JsonResponse(timeResponse)
    except:
        return HttpResponseBadRequest("Invalid Compartment Number")
    

def prescriptions(request,cpt_number):
    try:
        prescriptionDetails = MedicineCompartment.objects.filter(compartmentNo = cpt_number)[0]
        cpt_details ={
        "cptNumber" : prescriptionDetails.compartmentNo,
        "name" : prescriptionDetails.medicineName,
        "color" : prescriptionDetails.medicineColor,
        "time" : formatTimeIntoArray(prescriptionDetails.timeArray),
        "pickerTime" : formatTimeForPicker(str(prescriptionDetails.timeArray))
        }
        print getUpcomingMedicine(formatTimeIntoArray(prescriptionDetails.timeArray))
    except:
        return HttpResponseBadRequest("Invalid Compartment Number")
    return render(request,'vitalsigns/prescriptions.html',{'cpt_details':cpt_details})

def stream(request):
    return render(request,'vitalsigns/stream.html')

def getRawStream(request):
    return StreamingHttpResponse(gen(VideoCamera()), content_type='multipart/x-mixed-replace; boundary=frame')

def getPrescription(request,cpt_number):
    try:
        prescriptionDetails = MedicineCompartment.objects.filter(compartmentNo = cpt_number)[0]
        cpt_details ={
        "cptNumber" : prescriptionDetails.compartmentNo,
        "name" : prescriptionDetails.medicineName,
        "color" : prescriptionDetails.medicineColor,
        "time" : formatTimeIntoArray(prescriptionDetails.timeArray),
        "pickerTime" : formatTimeForPicker(str(prescriptionDetails.timeArray)),
        "upcomingTime" : getUpcomingMedicine(formatTimeIntoArray(prescriptionDetails.timeArray))
        }
    except:
        return HttpResponseBadRequest("Check POST syntax")
    return HttpResponse(json.dumps(cpt_details), content_type="application/json")

def updatePrescription(request):
    try:
        prescriptionDetails = MedicineCompartment.objects.filter(compartmentNo = int(request.POST['cpt-number']))[0]
        prescriptionDetails.medicineName = request.POST['medicineName']
        prescriptionDetails.medicineColor = request.POST['medicineColor']
        timeArray = ""
        for key,value in request.POST.iteritems():
            if key.startswith('time'):
                timeArray = timeArray + value + ","
        print timeArray
        prescriptionDetails.timeArray = timeArray
        prescriptionDetails.save()
    except:
        return HttpResponseBadRequest("Invalid Form Data Rajeev. Check correct cpt")
    return JsonResponse(request.POST)

def update(request):
    values = readSerially()
    print values
    #values = randomVitalSignsGenerator()
    extract = values.split(" ")
    toSend = {
        'bpm' : extract[0],
        'temp' : extract[1],
        'gx' : extract[2],
        'gy' : extract[3],
        'gz' : extract[4]
    }
    data = VitalSings(
        bpm = int(extract[0]),
        temp = int(float(extract[1])),
        gx = int(extract[2]),
        gy = int(extract[3]),
        gz = int(extract[4]),
        timeStamp = timezone.now()
    )
    data.save()
    #return HttpResponse("70")
    return HttpResponse(
        json.dumps(toSend),
        content_type = 'application/json; charset=utf8'
        )
# Assuming the following convention : heartbeat,temperature,gyro.x,gyro.y,gyro.z
# For serial reading, make sure arduino serial outputs separates each sensor value using commas in the same order as above. 

from random import randint

import serial

def randomVitalSignsGenerator():
    heartbeat = randint(50,90)
    temperature = randint(97,106)
    gyroX = randint(10000,20000)
    gyroY = randint(10000,20000)
    gyroZ = randint(10000,20000)
    serialOutput = str(heartbeat)+" "+str(temperature)+" "+str(gyroX)+" "+str(gyroY)+" "+str(gyroZ)
    return serialOutput
    return values

def readSerially():
	try:
		ser = serial.Serial('COM9', 57600)
		data = ser.readline()
	except:
		print "Serial Port not available, using random values"
	data = randomVitalSignsGenerator()
	return data